resource "aws_db_instance" "sqlserver" {
  allocated_storage    = 20
  engine               = "sqlserver-ex"
  engine_version       = "15.00.4073.23.v1"
  instance_class       = "db.t3.large"
  username             = "nailadmin"
  password             = "LYKnQfz9sx8qsHMe"
  identifier           = "nail"
  skip_final_snapshot  = true
  license_model        = "license-included"
  db_subnet_group_name = aws_db_subnet_group.sqlserver_subnet.name

  vpc_security_group_ids = [aws_security_group.sqlserver_sg.id]

  lifecycle {
    ignore_changes = [engine_version]
  }

  tags = merge(
    var.default-tags,
    var.environment-tags,
    {"Name" = "${var.environment}-sqlserver"}
  )
}

resource "aws_db_subnet_group" "sqlserver_subnet" {
  name       = "sqlserver-subnet"
  subnet_ids = [
    aws_subnet.private_subnet_1a.id,
    aws_subnet.private_subnet_1c.id
  ]

  tags = merge(
    var.default-tags,
    var.environment-tags,
    {"Name" = "${var.environment}-sqlserver-subnet"}
  )
}

resource "aws_security_group" "sqlserver_sg" {
  name        = "${var.environment}-sqlserver-sg"
  description = "Security group of sqlserver"
  vpc_id      = aws_vpc.main.id

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = [
      aws_vpc.main.cidr_block
    ]
  }

  tags = merge(
    var.default-tags,
    var.environment-tags,
    {"Name" = "${var.environment}-sqlserver-sg"}
  )
}