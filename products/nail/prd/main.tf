terraform {
  required_version = "= 0.14.8"

  backend "s3" {
    region         = "us-west-1"
    bucket         = "terraform-nail"
    key            = "nail/prd/terraform.tfstate"
    dynamodb_table = "terraform"
  }
}

provider "aws" {
  region  = "us-west-1"
}