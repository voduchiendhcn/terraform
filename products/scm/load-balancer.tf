# resource "aws_lb" "external_gateway" {
#   name               = "${var.environment}-external-gateway"
#   internal           = false
#   load_balancer_type = "application"
#   security_groups    = [aws_security_group.external_gateway_lb_sg.id]
#   subnets            = [
#     aws_subnet.public_subnet_1a.id,
#     aws_subnet.public_subnet_1b.id
#   ]

#   enable_deletion_protection = false # will change to true later
  
#   tags = merge(
#     var.default-tags,
#     var.environment-tags,
#     {"Name" = "${var.environment}-external-gateway"}
#   )
# }

# resource "aws_lb_listener" "http_listener" {
#   load_balancer_arn = aws_lb.external_gateway.arn
#   port              = "80"
#   protocol          = "HTTP"

#   default_action {
#     type             = "forward"
#     target_group_arn = aws_lb_target_group.http_external_gateway_tg.arn
#   }

#   depends_on = [aws_lb_target_group.http_external_gateway_tg]
# }

# resource "aws_security_group" "external_gateway_lb_sg" {
#   name        = "${var.environment}-external-gateway-sg"
#   description = "Security group of external gateway"
#   vpc_id      = aws_vpc.main.id

#   ingress {
#     description = "HTTPS from the Internet"
#     from_port   = 443
#     to_port     = 443
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   ingress {
#     description = "HTTP from the Internet"
#     from_port   = 80
#     to_port     = 80
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   tags = merge(
#     var.default-tags,
#     var.environment-tags,
#     {"Name" = "${var.environment}-external-gateway-sg"}
#   )
# }