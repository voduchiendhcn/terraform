resource "aws_subnet" "public_subnet_1a" {
  vpc_id                    = aws_vpc.main.id
  cidr_block                = "10.0.32.0/20"
  availability_zone         = "ap-southeast-1a"
  map_public_ip_on_launch   = true

  tags = merge(
    var.default-tags,
    var.environment-tags,
    {"Name" = "${var.environment}-public-subnet-1a"}
  )
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      tags
    ]
  }
}

resource "aws_eip" "ngw-ip" {
  vpc              = true

  tags = merge(
    var.default-tags,
    var.environment-tags,
    {"Name" = "${var.environment}-public-1a-eip"}
  )

  depends_on = [aws_internet_gateway.main_gw]
}

resource "aws_nat_gateway" "ngw" {
  allocation_id = aws_eip.ngw-ip.id
  subnet_id     = aws_subnet.public_subnet_1a.id

  tags = merge(
    var.default-tags,
    var.environment-tags,
    {"Name" = "${var.environment}-public-1a-ngw"}
  )
}

resource "aws_route_table" "public_1a_rtb" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main_gw.id
  }

  tags = merge(
    var.default-tags,
    var.environment-tags,
    {"Name" = "${var.environment}-public-1a-rtb"}
  )
}

resource "aws_route_table_association" "public_1a_rtb_association" {
  subnet_id      = aws_subnet.public_subnet_1a.id
  route_table_id = aws_route_table.public_1a_rtb.id
}

#-----------------------------------------------------------------------------

resource "aws_subnet" "public_subnet_1b" {
  vpc_id                    = aws_vpc.main.id
  cidr_block                = "10.0.96.0/20"
  availability_zone         = "ap-southeast-1b"
  map_public_ip_on_launch   = true

  tags = merge(
    var.default-tags,
    var.environment-tags,
    {"Name" = "${var.environment}-public-subnet-1b"}
  )

  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      tags
    ]
  }
}

resource "aws_route_table" "public_1b_rtb" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main_gw.id
  }

  tags = merge(
    var.default-tags,
    var.environment-tags,
    {"Name" = "${var.environment}-public-1b-rtb"}
  )
}

resource "aws_route_table_association" "public_1b_rtb_association" {
  subnet_id      = aws_subnet.public_subnet_1b.id
  route_table_id = aws_route_table.public_1b_rtb.id
}