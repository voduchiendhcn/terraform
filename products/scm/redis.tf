# resource "aws_elasticache_cluster" "redis" {
#   cluster_id           = "redis"
#   engine               = "redis"
#   node_type            = "cache.t3.small"
#   num_cache_nodes      = 1
#   parameter_group_name = "default.redis6.x"
#   engine_version       = "6.0.5"
#   port                 = 6379

#   subnet_group_name = aws_elasticache_subnet_group.redis_subnet.name
#   security_group_ids = [aws_security_group.redis_sg.id]

#   tags = merge(
#     var.default-tags,
#     var.environment-tags,
#     {"Name" = "${var.environment}-redis"}
#   )
# }

# resource "aws_elasticache_subnet_group" "redis_subnet" {
#   name       = "redis-subnet"
#   subnet_ids = [aws_subnet.private_subnet_1a.id]
# }

# resource "aws_security_group" "redis_sg" {
#   name        = "${var.environment}-redis-sg"
#   description = "Security group of redis"
#   vpc_id      = aws_vpc.main.id

#   ingress {
#     from_port   = 6379
#     to_port     = 6379
#     protocol    = "tcp"
#     cidr_blocks = [
#       aws_vpc.main.cidr_block
#     ]
#   }

#   tags = merge(
#     var.default-tags,
#     var.environment-tags,
#     {"Name" = "${var.environment}-redis-sg"}
#   )
# }
