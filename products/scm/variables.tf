variable "environment-tags" {
  description = "Define environment"
  type        = map(string)
  default     = {
    "Environment" = "Production"
  }
}

variable "environment" {
  type    = string
  default = "prd"
}

variable "default-tags" {
  description = "Define default tags for the resources"
  type        = map(string)
  default     = {
    "Project"     = "scm"
    "Handler"     = "bang.nguyen@scssolutions.io"
    "Managed-by"  = "Terraform"
  }
}

variable "project-name" {
  type    = string
  default = "scm"
}