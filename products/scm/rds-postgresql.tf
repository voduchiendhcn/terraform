# resource "aws_db_instance" "postgres" {
#   allocated_storage    = 10
#   engine               = "postgres"
#   engine_version       = "12.5"
#   instance_class       = "db.t3.micro"
#   name                 = "postgres"
#   username             = "postgres"
#   password             = "HelloSCM"
#   skip_final_snapshot  = true
#   multi_az             = false
#   identifier           = "postgres"
#   db_subnet_group_name = aws_db_subnet_group.postgres_subnet.name

#   vpc_security_group_ids = [aws_security_group.postgres_sg.id]

#   lifecycle {
#     ignore_changes = [engine_version]
#   }

#   tags = merge(
#     var.default-tags,
#     var.environment-tags,
#     {"Name" = "${var.environment}-postgres"}
#   )
# }

# resource "aws_db_subnet_group" "postgres_subnet" {
#   name       = "postgres-subnet"
#   subnet_ids = [
#     aws_subnet.private_subnet_1a.id,
#     aws_subnet.private_subnet_1b.id
#   ]

#   tags = merge(
#     var.default-tags,
#     var.environment-tags,
#     {"Name" = "${var.environment}-postgres-subnet"}
#   )
# }

# resource "aws_security_group" "postgres_sg" {
#   name        = "${var.environment}-postgres-sg"
#   description = "Security group of postgres"
#   vpc_id      = aws_vpc.main.id

#   ingress {
#     from_port   = 5432
#     to_port     = 5432
#     protocol    = "tcp"
#     cidr_blocks = [
#       aws_vpc.main.cidr_block
#     ]
#   }

#   tags = merge(
#     var.default-tags,
#     var.environment-tags,
#     {"Name" = "${var.environment}-postgres-sg"}
#   )
# }