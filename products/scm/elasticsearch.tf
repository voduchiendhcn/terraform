# resource "aws_elasticsearch_domain" "es" {
#   domain_name           = "elasticsearch"
#   elasticsearch_version = "6.7"

#   cluster_config {
#     instance_type = "t2.small.elasticsearch"
#   }

#   snapshot_options {
#     automated_snapshot_start_hour = 23
#   }

#   vpc_options {
#     subnet_ids = [aws_subnet.private_subnet_1a.id]
#     security_group_ids = [aws_security_group.es_sg.id]
#   }

#   ebs_options {
#     ebs_enabled = true
#     volume_size = 10
#   }

#   access_policies = <<CONFIG
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Effect": "Allow",
#       "Principal": {
#         "AWS": "*"
#       },
#       "Action": "es:*",
#       "Resource": "arn:aws:es:ap-southeast-1:607644936614:domain/elasticsearch/*"
#     }
#   ]
# }
# CONFIG

#   tags = merge(
#     var.default-tags,
#     var.environment-tags,
#     {"Name" = "${var.environment}-elasticsearch"}
#   )
  
#   depends_on = [aws_iam_service_linked_role.es]
# }

# resource "aws_security_group" "es_sg" {
#   name        = "${var.environment}-es-sg"
#   description = "Security group of elasticsearch"
#   vpc_id      = aws_vpc.main.id

#   ingress {
#     from_port   = 443
#     to_port     = 443
#     protocol    = "tcp"
#     cidr_blocks = [
#       aws_vpc.main.cidr_block
#     ]
#   }

#   tags = merge(
#     var.default-tags,
#     var.environment-tags,
#     {"Name" = "${var.environment}-elasticsearch-sg"}
#   )
# }

# resource "aws_iam_service_linked_role" "es" {
#   aws_service_name = "es.amazonaws.com"
# }