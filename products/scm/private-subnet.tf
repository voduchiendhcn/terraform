resource "aws_subnet" "private_subnet_1a" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.0.0.0/19"
  availability_zone = "ap-southeast-1a"

  tags = merge(
    var.default-tags,
    var.environment-tags,
    {"Name" = "${var.environment}-private-subnet-1a"}
  )

  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      tags
    ]
  }
}

resource "aws_route_table" "private_1a_rtb" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.ngw.id
  }

  tags = merge(
    var.default-tags,
    var.environment-tags,
    {"Name" = "${var.environment}-private-1a-rtb"}
  )
}

resource "aws_route_table_association" "private_1a_rtb_association" {
  subnet_id      = aws_subnet.private_subnet_1a.id
  route_table_id = aws_route_table.private_1a_rtb.id
}

#-----------------------------------------------------------------------------

resource "aws_subnet" "private_subnet_1b" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.0.64.0/19"
  availability_zone = "ap-southeast-1b"

  tags = merge(
    var.default-tags,
    var.environment-tags,
    {"Name" = "${var.environment}-private-subnet-1b"}
  )

  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      tags
    ]
  }
}