terraform {
  required_version = "= 0.14.8"

  backend "s3" {
    region         = "ap-southeast-1"
    bucket         = "scssolutions-terraform"
    key            = "prd/terraform.tfstate"
    dynamodb_table = "terraform"
  }
}

provider "aws" {
  region  = "ap-southeast-1"
}