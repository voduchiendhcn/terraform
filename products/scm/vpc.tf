resource "aws_vpc" "main" {
  cidr_block       = "10.0.0.0/16"

  tags = merge(
    var.default-tags,
    var.environment-tags,
    {"Name" = "${var.environment}-vpc"}
  )
}

resource "aws_internet_gateway" "main_gw" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    var.default-tags,
    var.environment-tags,
    {"Name" = "${var.environment}-igw"}
  )
}