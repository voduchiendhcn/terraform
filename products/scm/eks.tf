# resource "aws_security_group" "external_gateway_nodegroup_sg" {
#   name        = "${var.environment}-external-gateway-nodegroup-sg"
#   description = "Security group of external gateway"
#   vpc_id      = aws_vpc.main.id

#   ingress {
#     from_port   = 32443
#     to_port     = 32443
#     protocol    = "tcp"
#     security_groups = [aws_security_group.external_gateway_lb_sg.id]
#   }

#   ingress {
#     from_port   = 32080
#     to_port     = 32080
#     protocol    = "tcp"
#     security_groups = [aws_security_group.external_gateway_lb_sg.id]
#   }

#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   tags = merge(
#     var.default-tags,
#     var.environment-tags,
#     {"Name" = "${var.environment}-external-gateway-nodegroup-sg"}
#   )
# }